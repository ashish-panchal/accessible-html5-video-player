var AP = window.AP = window.AP || {};

AP.videoControls = (function () {
'use strict';

    // var CLS = {
    //     VideoPlayer             : '.video-player',
    //     ProgressbarTimeElapsed  : '.progressbar-time--elapsed',
    //     ProgressbarTimeDuration : '.progressbar-time--duration',
    //     ProgressbarProgress     : '.progressbar-progress',
    //     BtnPlyPause             : '.btn-play-pause',
    //     BtnStop                 : '.btn-stop',
    //     BtnVolume               : '.btn-volume',
    //     BtnCC                   : '.btn-cc',
    //     BtnFullscreen           : '.btn-fullscreen',
    //     VideoCustomControls     : '#video-custom-controls'
    // };
    // var CONST = {
    //     EMPTYSTR    : '',
    //     TRUE        : true,
    //     FALSE       : false,
    //     EXIT        : 'exit',
    //     FSARRAY     : 'fullscreenchange mozfullscreenchange webkitfullscreenchange msfullscreenchange'.split(' '),
    //     FSON        : 'fullScreenOn',
    //     FSOFF       : 'fullScreenOff',
    //     HTMLEVENTS  : 'HTMLEvents',
    //     STYLE       : 'style',
    //     CSSID       : 'video-custom-controls',
    //     CSSTYPE     : 'text/css',
    //     CSSMEDIA    : 'media',
    //     CSSRULE1    : 'video::-webkit-media-controls { display: none !important; }',
    //     CSSRULE2    : 'video::-webkit-media-controls-panel { display: none !important; }',
    //     CSSRULE3    : '.video-controls--wrapper { z-index: 2147483647; }'
    // };

    var bindEventsToUI = function (obj, callback) {

        // var VARS = {
        //     wrap            : obj,
        //     video           : VARS.wrap.querySelector(CLS.VideoPlayer),
        //     currentTime     : VARS.wrap.querySelector(CLS.ProgressbarTimeElapsed),
        //     videoDuration   : VARS.wrap.querySelector(CLS.ProgressbarTimeDuration),
        //     progressbar     : VARS.wrap.querySelector(CLS.ProgressbarProgress),
        //     btnPlayPause    : VARS.wrap.querySelector(CLS.BtnPlyPause),
        //     btnStop         : VARS.wrap.querySelector(CLS.BtnStop),
        //     btnVolume       : VARS.wrap.querySelector(CLS.BtnVolume),
        //     btnCC           : VARS.wrap.querySelector(CLS.BtnCC),
        //     btnFullScreen   : VARS.wrap.querySelector(CLS.BtnFullscreen),
        //     timeDrag        : CONST.TRUE,
        //     evt             : CONST.FSARRAY,
        //     evtLen          : VARS.evt.length,
        //     evtTriggered    : CONST.FSOFF,
        //     fsElement       : CONST.EMPTYSTR
        // };
        //
        // VARS.Video = CONST.FALSE;
        //
        // for(var i = 0; i < VARS.evtLen; i++) {
        //     document.addEventListener(VARS.evt[i], function () {
        //         VARS.fsElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
        //
        //         VARS.evtTriggered = VARS.fsElement ? CONST.FSON : CONST.FSOFF;
        //         VARS.evtTriggered === CONST.FSOFF && VARS.btnFullScreen.classList.remove(CONST.EXIT);
        //     });
        // }
        //
        // var fireEvent = function (ev, el) {
        //     var event = document.createEvent(CONST.HTMLEVENTS);
        //     event.initEvent(ev, CONST.TRUE, CONST.FALSE);
        //
        //     el.dispatchEvent(event);
        // };
        //
        // var getFullScreen = function () {
        //     if(VARS.video.requestFullscreen) {
        //         VARS.video.requestFullscreen();
        //     } else if(VARS.video.webkitRequestFullscreen) {
        //         VARS.video.webkitRequestFullscreen();
        //     } else if(VARS.video.webkitEnterFullscreen) {
        //         VARS.video.webkitEnterFullscreen();
        //     } else if(VARS.video.mozRequestFullScreen) {
        //         VARS.wrap.mozRequestFullScreen();
        //     } else if(VARS.video.msRequestFullscreen) {
        //         VARS.video.msRequestFullscreen();
        //     }
        //
        //     if(!document.querySelector(CLS.VideoCustomControls)) {
        //         var el = document.createElement(CONST.STYLE);
        //         el.id = CONST.CSSID;
        //         el.type = CONST.CSSTYPE,
        //         el.media = CONST.CSSMEDIA;
        //
        //         document.head.appendChild(el);
        //
        //         var style = document.querySelector(CLS.VideoCustomControls).sheet;
        //         style.insertRule(CONST.CSSRULE1, style.cssRules.length);
        //         style.insertRule(CONST.CSSRULE2, style.cssRules.length);
        //         style.insertRule(CONST.CSSRULE3, style.cssRules.length);
        //     }
        // };
        //
        // var exitFullScreen = function () {
        //     if(VARS.video.exitFullscreen) {
        //         VARS.video.exitFullscreen();
        //     } else if(document.webkitExitFullscreen) {
        //         document.webkitExitFullscreen();
        //     } else if(document.mozCancelFullScreen) {
        //         document.mozCancelFullScreen();
        //     } else if(VARS.video.msExitFullscreen) {
        //         VARS.video.msExitFullscreen();
        //     }
        // };



        var wrap = obj,
            vid = wrap.querySelector(".video-player"),
            currentTime = wrap.querySelector(".progressbar-time--elapsed"),
            videoDuration = wrap.querySelector(".progressbar-time--duration"),
            progressbar = wrap.querySelector(".progressbar-progress"),
            btnPlayPause = wrap.querySelector(".btn-play-pause"),
            btnStop = wrap.querySelector(".btn-stop"),
            btnVolume = wrap.querySelector(".btn-volume"),
            btnCC = wrap.querySelector(".btn-cc"),
            btnFullScreen = wrap.querySelector(".btn-fullscreen"),
            timeDrag = false,
            evt = "fullscreenchange mozfullscreenchange webkitfullscreenchange msfullscreenchange".split(" "),
            evtLen = evt.length,
            evtTriggered = "fullScreenOff";

        vid.controls = false;

        for(var i=0; i<evtLen; i++) {
            document.addEventListener(evt[i], function () {
                var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;

                evtTriggered = fullscreenElement ? "fullScreenOn" : "fullScreenOff";
                evtTriggered === "fullScreenOff" && btnFullScreen.classList.remove("exit");
            });
        }

        function fireEvent(ev, el) {
            var event = document.createEvent('HTMLEvents');
            event.initEvent(ev, true, false);

            el.dispatchEvent(event);
        }

        var getFullScreen = function () {
            if(vid.requestFullscreen) {
                vid.requestFullscreen();
            } else if(vid.webkitRequestFullscreen) {
                vid.webkitRequestFullscreen();
            } else if(vid.webkitEnterFullscreen) {
                vid.webkitEnterFullscreen();
            } else if(vid.mozRequestFullScreen) {
                wrap.mozRequestFullScreen();
            } else if(vid.msRequestFullscreen) {
                vid.msRequestFullscreen();
            }

            if(!document.querySelector("#video-custom-controls")) {
                var el = document.createElement("style");
                el.id = "video-custom-controls";
                el.type = "text/css",
                el.media = "screen";

                document.head.appendChild(el);

                var style = document.querySelector("#video-custom-controls").sheet;
                style.insertRule("video::-webkit-media-controls { display: none !important; }", style.cssRules.length);
                style.insertRule("video::-webkit-media-controls-panel { display: none !important; }", style.cssRules.length);
                style.insertRule(".video-controls--wrapper { z-index: 2147483647; }", style.cssRules.length);
            }
        };

        var exitFullScreen = function () {
            if(vid.exitFullscreen) {
                vid.exitFullscreen();
            } else if(document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(vid.msExitFullscreen) {
                vid.msExitFullscreen();
            }
        };

        var getTime = function (s) {
            var min = Math.floor(s / 60),
                sec = s - min * 60;

            sec = (sec < 9) ? "0" + sec.toFixed(0) : sec.toFixed(0);

            return min +":"+ sec;
        };

        var updatebar = function(x) {
            var maxduration = vid.duration,                         //Video duraiton
                position = x - wrap.getBoundingClientRect().x,                      //Click pos
                percentage = 100 * position / vid.getBoundingClientRect().width;

            //Check within range
            if(percentage > 100) {
                percentage = 100;
            } else if(percentage < 0) {
                percentage = 0;
            }

            //Update progressbar and video currenttime
            progressbar.value = percentage.toFixed(1);
            progressbar.innerHTML = percentage.toFixed(1) + "% played";
            vid.currentTime = maxduration * percentage / 100;
        };

        progressbar.addEventListener("mousedown", function (e) {
            timeDrag = true;
            updatebar(e.pageX);
        });

        document.addEventListener("mouseup", function (e) {
            if(timeDrag) {
                timeDrag = false;
                updatebar(e.pageX);
            }
        });

        document.addEventListener("mousemove", function (e) {
            if(timeDrag) {
                updatebar(e.pageX);
            }
        });

        vid.addEventListener('timeupdate', function() {
            var currentPos = vid.currentTime,                   //Get currenttime
                maxDuration = vid.duration,                     //Get video duration
                percentage = 100 * currentPos / maxDuration;    //in %

            currentTime.innerHTML = getTime(currentPos);
            videoDuration.innerHTML = getTime(maxDuration);
            progressbar.value = percentage.toFixed(1);
            progressbar.innerHTML = percentage.toFixed(1) + "% played";
        });

        vid.addEventListener("loadedmetadata", function () {
            videoDuration.innerHTML = getTime(vid.duration);
        });

        vid.addEventListener("ended", function () {
            fireEvent("click", btnStop);
            !!callback && callback();
        });

        !!btnPlayPause && btnPlayPause.addEventListener("click", function () {
            var className = "played";

            this.classList.toggle(className);
            this.parentNode.classList.toggle("playing");

            if(this.classList.contains(className)) {
                this.querySelector("span").innerHTML = "Pause";
                vid.play();
            } else {
                this.querySelector("span").innerHTML = "Play";
                vid.pause();
            }
        });

        !!btnStop && btnStop.addEventListener("click", function () {
            vid.currentTime = 0;
            vid.pause();
            btnPlayPause.classList.remove("played");
            btnPlayPause.querySelector("span").innerHTML = "Play";

            vid.muted = false;
            btnVolume.classList.remove("muted");
            btnVolume.querySelector("span").innerHTML = "Mute";

            this.parentNode.classList.remove("playing");
        });

        !!btnVolume && btnVolume.addEventListener("click", function () {
            var className = "muted";

            this.classList.toggle(className);

            if(this.classList.contains(className)) {
                this.querySelector("span").innerHTML = "Volume";
                vid.muted = true;
            } else {
                this.querySelector("span").innerHTML = "Mute";
                vid.muted = false;
            }
        });

        !!btnCC && btnCC.addEventListener("click", function () {
            var className = "on";

            this.classList.toggle(className);

            if(this.classList.contains(className)) {
                vid.textTracks[0].mode = "showing";
            } else {
                vid.textTracks[0].mode = "hidden";
            }
        });

        !!btnFullScreen && btnFullScreen.addEventListener("click", function () {
            var className = "exit";

            this.classList.toggle(className);

            if(this.classList.contains(className)) {
                getFullScreen();
            } else {
                exitFullScreen();
            }
        });

        setTimeout(function () {
            if (videoDuration.innerHTML == "0:00") {
                videoDuration.innerHTML = getTime(vid.duration);
            }
        }, 500);
    };

    var init = function (obj, callback) {
        var elements = document.querySelectorAll(obj),
            len = elements.length;

        for(var i = 0; i < len; i++) {
            bindEventsToUI(elements[i], callback);
        }
    };

    return {
        init: init
    }
}());
